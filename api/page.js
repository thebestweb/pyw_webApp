import { http } from '../utils'

export const web_teachers = data => http.post('/web_teacher/list', data)

export const web_teacher_count = data => http.post('/web_teacher/count', data)

export const web_teacher_detail = data => http.post('/web_teacher/detail', data)