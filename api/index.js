import Vue from 'vue'

import * as page from './page'

const api = Object.assign({}, page)

Vue.prototype.$api = api

export default api